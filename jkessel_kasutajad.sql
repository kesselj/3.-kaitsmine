-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 26, 2014 at 05:25 PM
-- Server version: 5.5.37
-- PHP Version: 5.3.10-1ubuntu3.11

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `jkessel_kasutajad`
--

CREATE TABLE IF NOT EXISTS `jkessel_kasutajad` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `kasutajanimi` varchar(255) DEFAULT NULL,
  `parool` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `kasutajanimi` (`kasutajanimi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `jkessel_kasutajad`
--

INSERT INTO `jkessel_kasutajad` (`ID`, `kasutajanimi`, `parool`) VALUES
(1, 'joonas', 'kessel'),
(2, 'tanel', '4f36b836ccc071ac3b0e5a38a2246843f0f2b2eb'),
(4, 'kessel', 'a461863938ff70025ca804869934e25959731183'),
(6, 'jobu', '1bdefa3e3443b960a8273c778ce9becc98b15b14'),
(7, 'kert', '1ea30778aaa07f939a82d4033dfea4120fac93a1'),
(8, 'kristo', '31cc0cbf2a284c9e9ab9489a1b9d091fc8f6c726'),
(9, 'daniel', '1afb4e7d8f1d28d3945bfed25cb6f71e8bf9ecd5');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
